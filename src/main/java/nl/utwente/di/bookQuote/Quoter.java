package nl.utwente.di.bookQuote;

import java.util.HashMap;

public class Quoter {
     double getBookPrice(String isbn){
         HashMap<Integer, Double> ret=  new HashMap<>();
         ret.put(1, 10.0);
         ret.put(2, 45.0);
         ret.put(3, 20.0);
         ret.put(4, 35.0);
         ret.put(5, 50.0);
         if(ret.containsKey(isbn)){
            return ret.get(isbn);
         } else {
             return 0.0;
         }
     }
}
